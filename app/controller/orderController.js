const db = require("../config/db.js");
const config = require("../config/config.js");
const Order = db.order;
const asyncMiddleware = require("express-async-handler");

exports.addOrder = asyncMiddleware(async (req, res) => {
    // Save User to Database
    //console.log("Processing func -> SignUp");
    if (req.body.userid)
    {
        await Order.create({
            userId: req.body.userid,
            bookId: req.body.bookid
        });
    
        //   const roles = await Role.findAll({
        //     where: {
        //       name: {
        //         [Op.or]: req.body.roles
        //       }
        //     }
        //   });
    
        //   await user.setRoles(roles);
        //   await user.setRoles(roles, {through : {status : "unblock"}});
    
        res.status(201).send({
            status: "Order has been created!"
        });
    }else{
    

    res.status(200).json({
        Error : "User ID must be input"
    });
    }
});

exports.viewOrder = asyncMiddleware(async (req, res) => {
    const order = await Order.findAll();
    res.status(200).json({
        description: "View Orders",
        data: order
    });
});

exports.viewOrderId = asyncMiddleware(async (req, res) => {
    const order = await Order.findAll({
        where: {
            userId: req.params.id
        }
    }
    );
    res.status(200).json({
        description: "View One Order",
        data: order
    });
});
