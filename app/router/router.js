const verifySignUp = require("./verifySignUp");
const authJwt = require("./verifyJwtToken");
const authController = require("../controller/authController.js");
const userController = require("../controller/userController.js");
const bookController = require("../controller/bookController.js");
const orderController = require("../controller/orderController.js");

module.exports = function (app) {
  // Auth
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUserNameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    authController.signup
  );
  app.post("/api/auth/signin", authController.signin);

  // get all user
  app.get("/api/users", [authJwt.verifyToken], userController.users);
  // get 1 user according to roles
  app.get("/api/test/user", [authJwt.verifyToken], userController.userContent);
  app.get(
    "/api/test/pm",
    [authJwt.verifyToken, authJwt.isPmOrAdmin],
    userController.managementBoard
  );
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.adminBoard
  );
  app.post("/api/books", [authJwt.verifyToken], bookController.addBook);
  app.get("/api/books", [authJwt.verifyToken], bookController.viewBook);
  app.get("/api/books/:id", [authJwt.verifyToken], bookController.viewBookId);
  app.put("/api/books/:id", [authJwt.verifyToken], bookController.editBook);


  app.post("/api/order/", [authJwt.verifyToken], orderController.addOrder);
  app.get("/api/order/", [authJwt.verifyToken], orderController.viewOrder);
  app.get("/api/order/:id", [authJwt.verifyToken], orderController.viewOrderId);

  // error handler 404
  app.use(function (req, res, next) {
    return res.status(404).send({
      status: 404,
      message: "Not Found"
    });
  });
  // error handler 500
  app.use(function (err, req, res, next) {
    return res.status(500).send({
      file: "/router/router.js",
      error: err
    });
  });
};