module.exports = (sequelize, Sequelize) => {
    const Order = sequelize.define('order_books', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }
    });
    return Order;
}